package main

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"os"
	"bytes"
	"mime/multipart"
	"io"
)

var (

	server = "localhost:27017"

	dbname = "yawoen"

	docname = "companies"
)

func TestRepository_AddCompanyRepository_AddCompany(t *testing.T) {

	testCompany := Company{}
	testCompany.AddressZip = "12345"
	testCompany.Name = "Microsoft"
	testCompany.Website = "www.microsoft.com"

	session, err := mgo.Dial(server)
	defer session.Close()
	testCompany.ID = bson.NewObjectId()
	session.DB(dbname).C(docname).Insert(testCompany)
	if err != nil {
		log.Fatal(err)
		return
	}
	log.Println("Company created")

}

func TestRepository_FindByNameAndZip(t *testing.T) {

	session, err := mgo.Dial(server)
	if err != nil {
		log.Fatal("Failed to establish connection to Mongo server:", err)
	}
	defer session.Close()
	results := Companies{}
	c := session.DB(dbname).C(docname)
	search := bson.RegEx{"Mic.*", ""}
	c.Find(bson.M{"name": search, "addresszip" : "12345" }).All(&results)
	if results != nil{
		log.Println(results)
	}

}

func TestRepository_GetCompanies(t *testing.T) {
	session, err := mgo.Dial(server)
	if err != nil {
		log.Fatal("Failed to establish connection to Mongo server:", err)
	}
	defer session.Close()
	c := session.DB(dbname).C(docname)
	results := Companies{}
	if err := c.Find(nil).All(&results); err != nil {
		log.Fatal("Failed to write results:", err)
	}
	if results != nil{
		log.Println("Companies retrieved")
	}
}

func TestController_MergeCompany(t *testing.T) {
	csvFile, err := os.Open("csv/q2_clientData.csv")
	if err != nil {
		log.Println(err)
	}
	defer csvFile.Close()
	requestPart := &bytes.Buffer{}
	writer := multipart.NewWriter(requestPart)
	part, err := writer.CreateFormFile("csv", "csv/temp.csv")
	if err != nil {
		log.Println(err)
	}
	_, err = io.Copy(part, csvFile)

	err = writer.Close()
	if err != nil {
		log.Println(err)
	}

	req, _ := http.NewRequest("POST", "/v1/data", requestPart)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	router := NewRouter()
	response := httptest.NewRecorder()
	router.ServeHTTP(response, req)
	if response.Code == http.StatusOK {
		t.Log("Companies merged")
	} else {
		t.Errorf("Expected response code %d. Got %d\n", http.StatusOK, response.Code)
	}
}

func TestController_MergeEmptyCompany(t *testing.T) {
	csvFile, err := os.Open("csv/empty.csv")
	if err != nil {
		log.Println(err)
	}
	defer csvFile.Close()
	requestPart := &bytes.Buffer{}
	writer := multipart.NewWriter(requestPart)
	part, err := writer.CreateFormFile("csv", "csv/empty-temp.csv")
	if err != nil {
		log.Println(err)
	}
	_, err = io.Copy(part, csvFile)

	err = writer.Close()
	if err != nil {
		log.Println(err)
	}

	req, _ := http.NewRequest("POST", "/v1/data", requestPart)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	router := NewRouter()
	response := httptest.NewRecorder()
	router.ServeHTTP(response, req)
	if response.Code == http.StatusBadRequest {
		t.Log("Companies merged")
	} else {
		t.Errorf("Expected response code %d. Got %d\n", http.StatusBadRequest, response.Code)
	}
}


