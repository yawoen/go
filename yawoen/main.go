package main

import (
	"github.com/gorilla/handlers"
	"log"
	"net/http"
)

// Load data from csv file on startup
func init() {
	//util := Util{}
	//util.loadCsvData()
}

func main() {

	router := NewRouter()
	allowedOrigins := handlers.AllowedOrigins([]string{"*"})
	allowedMethods := handlers.AllowedMethods([]string{"GET", "POST", "DELETE", "PUT"})
	log.Fatal(http.ListenAndServe(":9000",
		handlers.CORS(allowedOrigins, allowedMethods)(router)))
}
